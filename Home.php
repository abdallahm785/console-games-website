
<!DOCTYPE html>
<head>
    <title>Sony consoles</title>
    <link rel="icon" href="Images/th.jpeg">
    <link rel="stylesheet" href="assets/animate.css">
    <link rel="stylesheet" href="assets/owl.carousel.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

</head>
<body style="opacity: 150% ; background-image: url(Images/motion_stripes-wide.jpg)">
<main>
    <?php
    include 'Nav.php';
    ?>
    <div class="container-fluid" style="width: 600px">
        <div id="carouselExampleIndicators" class="carousel slide " data-bs-ride="carousel" >
            <div class="carousel-indicators" style="padding-bottom: 200px">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>

            <div class="carousel-inner">
                <div class="carousel-item active" style="height: 800px ;">
                    <h1 style=" margin-right:200px ; color: white ; background-size: cover;position: relative ; top: 500px; left: 100px" >Call of duty : MW </h1>
                    <img style="width: 50px;height: 400px" src="Images/call-of-duty-modern-warfare.jpg" class=" d-block w-100 img-responsive "  alt="...">

                </div>
                <div class="carousel-item"style="height: 800px ;">
                    <h1 style="color: white ; background-size: cover ; position: relative ; top: 500px; left: 100px" >Call of duty : Cold War</h1>
                    <img style="text-align: center;width: 50px;height: 400px" src="Images/Cod cw.jpeg" class=" d-block w-100 img-responsive" alt="...">

                </div>
                <div class="carousel-item" style="height: 800px ;">
                    <h1 style="text-align: center ; color: white ; background-size: cover ; position: relative ; top: 500px; left: 10px" >FIFA 21</h1>

                    <img style="width: 50px;height: 400px" src="Images/Fifa 21.jpeg" class=" d-block w-100 img-responsive" alt="...">


                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"  data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"  data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

    </div>

    <div style="margin-bottom: 200px ; color: black ; background-color: whitesmoke"  class="container">
        <h1>Game Valley</h1>
        <p>Online gaming, entertainment, friends, shopping and more – add friends and enjoy incredible gaming and entertainment on your PlayStation consoles and other connected devices,for more information click <a
                    href="https://www.game.co.uk/en/games/playstation-4-ps4-games/?cm_sp=PlayStationFormatHub-_-Games-_-ShopAllGames#Page2">here</a></u></p>
    </div>
    <footer style="background-color: midnightblue ; color: white; text-align: center ;padding: 20px">
        © 2021 Sony Interactive Entertainment LLC
    </footer>
</main>


<script src="assets/owl.carousel.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>