<?php?>
<!DOCTYPE html>
<head>
    <title>Sony consoles</title>
    <link rel="icon" href="Images/th.jpeg">
    <link rel="stylesheet" href="assets/animate.css">
    <link rel="stylesheet" href="assets/owl.carousel.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <style>

    </style>
</head>
<body style="opacity: 150% ; background-image: url(Images/motion_stripes-wide.jpg)">
<main>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand " href="Home.php">PS Games</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="Home.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a style="color: white" class="nav-link" href="games.php">Games</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a style="color: white" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            More
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="RankingTable.php">Ranking</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="lognew.php"> Log in</a></li>
                            <li><a class="dropdown-item" href="regnew.php">Account</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
</main>
</body>
</html>