<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
    <title>
        Registration
    </title>
    <link rel="stylesheet" type="text/css"
          href="css.css">
</head>

<body style="opacity: 150% ; background-image: url(Images/motion_stripes-wide.jpg)">
<div class="header">
    <h2>Register</h2>
</div>

<form method="post" action="regnew.php">

    <?php include('error.php'); ?>

    <div class="input-group">
        <label>Enter Fname</label>
        <input type="text" name="fname"
               value="<?php echo $fname; ?>">
    </div>
    <div class="input-group">
        <label>Enter Last name</label>
        <input type="text" name="lname"
               value="<?php echo $lname; ?>">
    </div>
    <div class="input-group">
        <label>Email</label>
        <input type="email" name="email"
               value="<?php echo $email; ?>">
    </div>
    <div class="input-group">
        <label>Enter Password</label>
        <input type="password" name="password">
    </div>
    <div class="input-group">
        <button type="submit" class="btn"
                name="reg_user">
            Register
        </button>
    </div>
    <p>
        Already having an account?
        <a href="lognew.php">
            Login Here!
        </a>
    </p>
</form>
</body>
</html>