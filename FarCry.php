<?php?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="Images/th.jpeg">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <title>Far Cry 5</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

        fieldset, label { margin: 0; padding: 0; }
        body{ margin: 20px; }
        h1 { font-size: 1.5em; margin: 10px; }

        /****** Style Star Rating Widget *****/

        .rating {
            border: none;
            float: left;
        }

        .rating > input { display: none; }
        .rating > label:before {
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
        }

        .rating > .half:before {
            content: "\f089";
            position: absolute;
        }

        .rating > label {
            color: #ddd;
            float: right;
        }

        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label { color: #FFED85;  }
        *  {
            box-sizing: border-box;
        }


        .fa {
            font-size: 25px;
        }


        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        }
        body{
            background:#eee;
        }

        .ibox {
            clear: both;
            margin-bottom: 25px;
            /*padding-top: 50px;*/
        }
        .ibox.collapsed .ibox-content {
            display: none;
        }
        .ibox.collapsed .fa.fa-chevron-up:before {
            content: "\f078";
        }
        .ibox.collapsed .fa.fa-chevron-down:before {
            content: "\f077";
        }
        .ibox:after,
        .ibox:before {
            display: table;
        }
        .ibox-title {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            background-color: #ffffff;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 3px 0 0;
            color: inherit;
            margin-bottom: 0;
            padding: 14px 15px 7px;
            min-height: 48px;
        }
        .ibox-title h5 {
            display: inline-block;
            font-size: 14px;
            margin: 0 0 7px;
            padding: 0;
            text-overflow: ellipsis;
            float: left;
        }
        .fa-star:hover{
            color: red;

        }

        .ibox-tools a {
            cursor: pointer;
            margin-left: 5px;
            color: #c4c4c4;
        }
        .ibox-tools {
            display: block;
            float: none;
            margin-top: 0;
            position: relative;
            padding: 0;
            text-align: right;
        }
        .ibox-content {
            background-color: #ffffff;
            color: inherit;
            padding: 15px 20px 20px 20px;
            border-color: #e7eaec;
            border-image: none;
            border-style: solid solid none;
            border-width: 1px 0;
        }
        .ibox-footer {
            color: inherit;
            border-top: 1px solid #e7eaec;
            font-size: 90%;
            background: #ffffff;
            padding: 10px 15px;
        }


    </style>
</head>
<body style="opacity: 150% ; background-image: url(Images/motion_stripes-wide.jpg) ;padding: 0 ; margin: 0">

<?php
include 'Nav.php';
?>
<div class="container-fluid" style="width: 600px">
    <div id="carouselExampleIndicators" class="carousel slide " data-bs-ride="carousel" >
        <div class="carousel-indicators" style="padding-top: 400px">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
        </div>

        <div style="padding-top: 50px" class="carousel-inner">
            <div class="carousel-item active" style="height: 400px ;">

                <img style="width: 50px;height: 400px" src="Images/Farcry5.jpeg" class=" d-block w-100 img-responsive "  alt="...">

            </div>
            <div class="carousel-item"style="height: 400px ;">

                <img style="width: 50px;height: 400px" src="FarCry/far-cry-5-ending-joseph-seed.jpg" class=" d-block w-100 img-responsive" alt="..." controls>

            </div>
            <div class="carousel-item" style="height: 400px ">

                <img style="width: 50px;height: 400px" src="FarCry/Far-Cry-5-Cult-Shot-05.jpg" class=" d-block w-100 img-responsive" alt="...">

            </div>
            <div class="carousel-item" style="height: 400px ">

                <img style="width: 50px;height: 400px" src="FarCry/review-screenshot-6.jpg" class=" d-block w-100 img-responsive" alt="...">

            </div>
        </div>

    </div>
</div>
<div class="row" style="padding-top: 50px">
    <div  class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-content profile-content">
                <h4><strong>Far Cry 5</strong></h4>
                <p><i class="fa fa-clock-o"></i> Released on Mar 27 , 2018</p>
                <p>
                    Far Cry 5 is a 2018 first-person shooter game developed by Ubisoft Montreal and Ubisoft Toronto and published by Ubisoft.
                    It is the fifth main installment in the Far Cry series.
                    <br>
                    <br>
                    The game takes place in Hope County, a fictional region of Montana, United States.
                    The main story revolves around the Project at Eden's Gate,a doomsday cult that has taken over the county at the command of its
                    charismatic and powerful leader, Joseph Seed.Players control an unnamed junior deputy sheriff who becomes trapped in Hope County and
                    must work alongside factions of a resistance to liberate the county from the despotic rule of the Seeds and Eden's Gate.
                    Gameplay focuses on combat and exploration; players battle enemy soldiers and dangerous wildlife using a wide array of weapons.
                    The game features many elements found in role-playing games, such as a branching storyline and side quests.
                    The game also features a map editor, a co-operative multiplayer mode, and a competitive multiplayer mode.
                    <br>
                    The game was released in March 2018 for Microsoft Windows, PlayStation 4 and Xbox One.
                    Far Cry 5 was met with mostly positive critical reception upon release.
                    <br>

                </p>
                <div class="row m-t-md">

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="background-color: white;height: 470px">
        <h3 style="text-align: center; margin-top: 25px; ">Write your review here</h3>
        <label for="exampleFormControlTextarea1" class="form-label">your comment</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        <br><br>
        <h3>Add your rating</h3>
        <fieldset class="rating">
            <input type="radio" id="star10" name="rating" value="10" />
            <label class = "full" for="star10" title="Awesome - 10 stars"></label>
            <input type="radio" id="star9half" name="rating" value="9 and a half" />
            <label class="half" for="star9half" title="Awesome - 9.5 stars"></label>
            <input type="radio" id="star9" name="rating" value="9" />
            <label class = "full" for="star9" title="Awesome - 9 stars"></label>
            <input type="radio" id="star8half" name="rating" value="8 and a half" />
            <label class="half" for="star8half" title="Pretty good - 8.5 stars"></label>
            <input type="radio" id="star8" name="rating" value="8" />
            <label class = "full" for="star8" title="Pretty good - 8 stars"></label>
            <input type="radio" id="star7half" name="rating" value="7 and a half" />
            <label class="half" for="star7half" title="Pretty good - 7.5 stars"></label>
            <input type="radio" id="star7" name="rating" value="7" />
            <label class = "full" for="star7" title="Pretty good - 7 stars"></label>
            <input type="radio" id="star6half" name="rating" value="6 and a half" />
            <label class="half" for="star6half" title="good - 6.5 stars"></label>
            <input type="radio" id="star6" name="rating" value="6" />
            <label class = "full" for="star6" title="good - 6 star"></label>
            <input type="radio" id="star5half" name="rating" value="5 and a half" />
            <label class="half" for="star5half" title="good - 5.5 stars">
            </label> <input type="radio" id="star5" name="rating" value="5" />
            <label class = "full" for="star5" title="good - 5 stars"></label>
            <input type="radio" id="star4half" name="rating" value="4 and a half" />
            <label class="half" for="star4half" title="Not bad-4.5 stars"></label>
            <input type="radio" id="star4" name="rating" value="4" />
            <label class = "full" for="star4" title="Not bad - 4 stars"></label>
            <input type="radio" id="star3half" name="rating" value="3 and a half" />
            <label class="half" for="star3half" title="Not bad - 3.5 stars"></label>
            <input type="radio" id="star3" name="rating" value="3" />
            <label class = "full" for="star3" title="Not bad - 3 stars"></label>
            <input type="radio" id="star2half" name="rating" value="2 and a half" />
            <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
            <input type="radio" id="star2" name="rating" value="2" />
            <label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
            <input type="radio" id="star1half" name="rating" value="1 and a half" />
            <label class="half" for="star1half" title="bad - 1.5 stars"></label>
            <input type="radio" id="star1" name="rating" value="1" />
            <label class = "full" for="star1" title="bad - 1 star"></label>
            <input type="radio" id="starhalf" name="rating" value="half" />
            <label class="half" for="starhalf" title="Very bad - 0.5 stars"></label>
        </fieldset>
        <br>

        <button style="float:right" class="btn btn-primary mb-3 " type="submit">Submit</button>
    </div>

</div>
<div class="container-fluid" style="background-color: white;margin-bottom: 20px ; border: solid 1px ; width: 350px ; height: 400px ; text-align: start; background-color: black ;">
    <h3 class="text-decoration-underline" style="color: white ;text-align: start ; padding-bottom: 20px"> 3 Official Retailers</h3>
    <div style="color: white" class="row">
        <h4>Playstation Store</h4>
        <p> Available <a
                href="https://store.playstation.com/en-us/product/UP0001-CUSA05904_00-FARCRY5GAME00000">here</a></u></p>
    </div>
    <hr>
    <div style="color: white" class="row">
        <h4>Amazon</h4>
        <p> Available <a
                href="https://www.amazon.com/Far-Cry-5-PS4-playstation-4/dp/B07L3YGJMY/ref=sr_1_1?adgrpid=1345802796366126&dchild=1&hvadid=84112745732483&hvbmt=be&hvdev=c&hvlocphy=187&hvnetw=o&hvqmt=e&hvtargid=kwd-84113019648466%3Aloc-187&hydadcr=19754_10565464&keywords=ps4+games+far+cry+5&qid=1616340384&sr=8-1"
                      >here</a></u></p>
    </div>
    <hr>
    <div style="color: white" class="row">
        <h4> Ubisoft </h4>
        <p> Available <a
                href="https://store.ubi.com/us/game/?lang=en_US&pid=591567f6ca1a6460388b4568&dwvar_591567f6ca1a6460388b4568_Platform=ps4&edition=Standard%20Edition&source=detail">here</a></u></p>
    </div>
</div>


<footer style="background-color: midnightblue ; color: white; text-align: center ;padding: 20px"> © 2021 Sony Interactive Entertainment LLC
</footer>

</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

</body>
