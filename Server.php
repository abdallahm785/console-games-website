<?php

// Starting the session, necessary
// for using session variables
session_start();

// Declaring and hoisting the variables
$fname = "";
$lname = "";
$email = "";
$errors = array();
$_SESSION['success'] = "";
$comment= "";
$rating= "";

// DBMS connection code -> hostname,
$revv = mysqli_connect('localhost', 'root', '', 'register');

if (isset($_POST['rev'])){
    $comment = mysqli_real_escape_string($revv, $_POST['comment']);
    $rating = mysqli_real_escape_string($revv, $_POST['rating']);
    if (empty($comment)) { array_push($errors, "comment is required"); }
    if (empty($rating)) { array_push($errors, "rating is required"); }
    header('location: account.php');


}
// username, password, database name

$db = mysqli_connect('localhost', 'root', '', 'register');


// Registration code
if (isset($_POST['reg_user'])) {

    // Receiving the values entered and storing
    // in the variables
    // Data sanitization is done to prevent
    // SQL injections
    $fname = mysqli_real_escape_string($db, $_POST['fname']);
    $lname = mysqli_real_escape_string($db, $_POST['lname']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    // Ensuring that the user has not left any input field blank
    // error messages will be displayed for every blank input
    if (empty($fname)) { array_push($errors, "fname is required"); }
    if (empty($lname)) { array_push($errors, "lname is required"); }
    if (empty($email)) { array_push($errors, "Email is required"); }
    if (empty($password)) { array_push($errors, "Password is required"); }

    // If the form is error free, then register the user

    // Storing username of the logged in user,
    // in the session variable
    $_SESSION['fname'] = $fname;

    // Welcome message
    $_SESSION['success'] = "";

    // Page on which the user will be
    // redirected after logging in
    header('location: index.php');

}

// User login
if (isset($_POST['login_user'])) {

    // Data sanitization to prevent SQL injection
    $fname = mysqli_real_escape_string($db, $_POST['fname']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    // Error message if the input field is left blank
    if (empty($fname)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }

    // Checking for the errors
    if (count($errors) == 0) {

        // Password matching
        $password = md5($password);

        $query = "SELECT * FROM users WHERE fname=
				'$fname' AND password='$password'";
        $results = mysqli_query($db, $query);

        // $results = 1 means that one user with the
        // entered username exists
        if (mysqli_num_rows($results) == 1) {

            // Storing username in session variable
            $_SESSION['fname'] = $fname;

            // Welcome message
            $_SESSION['success'] = "You have logged in!";

            // Page on which the user is sent
            // to after logging in
            header('location: index.php');
        }
        else {

            // If the username and password doesn't match
            array_push($errors, "Username or password incorrect");
        }
    }
}

?>