<?php

// Starting the session, to use and
// store data in session variable
session_start();

// If the session variable is empty, this
// means the user is yet to login
// User will be sent to 'login.php' page
// to allow the user to login
if (!isset($_SESSION['fname'])) {
    $_SESSION['msg'] = "You have to log in first";
    header('location: lognew.php');
}

// Logout button will destroy the session, and
// will unset the session variables
// User will be headed to 'login.php'
// after loggin out
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['fname']);
    header("location: lognew.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Homepage</title>
    <link rel="stylesheet" type="text/css"
          href="CSS%20File.css">
    <link rel="icon" href="Images/th.jpeg">
    <link rel="stylesheet" href="assets/animate.css">
    <link rel="stylesheet" href="assets/owl.carousel.min.css">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

</head>
<body style="opacity: 150% ; background-image: url(Images/motion_stripes-wide.jpg)">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="position: fixed; top: 0;width: 100%;z-index: +1">
    <div class="container-fluid">
        <a class="navbar-brand " href="file1.php">PS Games</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="file1.php">Home</a>
                </li>
                <li class="nav-item">
                    <a style="color: white" class="nav-link" href="games.php">Games</a>
                </li>
                <li class="nav-item dropdown">
                    <a style="color: white" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        More
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="reviews.php">Reviews</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="">Account</a></li>
                    </ul>
                </li>
                <li class="nav-item" style="position: fixed; left: 1350px">


                    <!-- Creating notification when the
                            user logs in -->

                    <!-- Accessible only to the users that
                            have logged in already -->
                    <!--                        --><?php //if (isset($_SESSION['success'])) : ?>
                    <!--                            <div class="error success" >-->
                    <!--                                <h3>-->
                    <!--                                    --><?php
                    //                                    echo $_SESSION['success'];
                    //                                    unset($_SESSION['success']);
                    //                                    ?>
                    <!--                                </h3>-->
                    <!--                            </div>-->
                    <!--                        --><?php //endif ?>

                    <!-- information of the user logged in -->
                    <!-- welcome message for the logged in user -->
                    <?php if (isset($_SESSION['fname'])) : ?>
                    <a href="#" class="nav-link">
                        Welcome
                        <strong style="color: whitesmoke">
                            <?php echo $_SESSION['fname']; ?>



                        </strong>
                    </a>
                </li>
                <li class="nav-item" style="position: fixed; right: 20px">
                    <a href="index.php?logout='1'" style="color: #47d8ff;" class="nav-link">
                        Logout
                    </a>
                    <?php endif ?>
                </li>
            </ul>

        </div>
    </div>
</nav>
<br>
<br>
<br>

<div class="container-fluid" style="width: 600px">
    <div id="carouselExampleIndicators" class="carousel slide " data-bs-ride="carousel" >
        <div class="carousel-indicators" style="padding-bottom: 200px">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>

        <div class="carousel-inner">
            <div class="carousel-item active" style="height: 800px ;">
                <h1 style=" margin-right:200px ; color: white ; background-size: cover;position: relative ; top: 500px; left: 100px" >Call of duty : MW </h1>
                <img style="width: 50px;height: 400px" src="Images/call-of-duty-modern-warfare.jpg" class=" d-block w-100 img-responsive "  alt="...">

            </div>
            <div class="carousel-item"style="height: 800px ;">
                <h1 style="color: white ; background-size: cover ; position: relative ; top: 500px; left: 100px" >Call of duty : Cold War</h1>
                <img style="text-align: center;width: 50px;height: 400px" src="Images/Cod cw.jpeg" class=" d-block w-100 img-responsive" alt="...">

            </div>
            <div class="carousel-item" style="height: 800px ;">
                <h1 style="text-align: center ; color: white ; background-size: cover ; position: relative ; top: 500px; left: 10px" >FIFA 21</h1>

                <img style="width: 50px;height: 400px" src="Images/Fifa 21.jpeg" class=" d-block w-100 img-responsive" alt="...">


            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"  data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"  data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

</div>

<div style="margin-bottom: 200px ; color: black ; background-color: whitesmoke"  class="container">
    <h1>Game Valley</h1>
    <p>Online gaming, entertainment, friends, shopping and more – add friends and enjoy incredible gaming and entertainment on your PlayStation consoles and other connected devices,for more information click <a
                href="https://www.game.co.uk/en/games/playstation-4-ps4-games/?cm_sp=PlayStationFormatHub-_-Games-_-ShopAllGames#Page2">here</a></u></p>
</div>
<!--<form method="post" style="background-color: white;height: 470px;box-shadow: 2px 3px rgba(81,81,81,0.62);padding-top: 10px ;width: 80%">-->
<!---->
<!--    <h3 style="text-align: center; margin-top: 25px; ">Write your review here</h3>-->
<!--    <label for="comment" class="form-label"  >your comment</label>-->
<!--    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" value="--><?php //echo $comment; ?><!--" ></textarea>-->
<!--    <br><br>-->
<!--    <h3>Add your rating</h3>-->
<!---->
<!--    <fieldset class="rating" for="rating">-->
<!--        <input type="radio" id="star10" name="rating" value="10" />-->
<!--        <label class = "full" for="star10" title="Awesome - 10 stars"></label>-->
<!--        <input type="radio" id="star9half" name="rating" value="9 and a half" />-->
<!--        <label class="half" for="star9half" title="Awesome - 9.5 stars"></label>-->
<!--        <input type="radio" id="star9" name="rating" value="9" />-->
<!--        <label class = "full" for="star9" title="Awesome - 9 stars"></label>-->
<!--        <input type="radio" id="star8half" name="rating" value="8 and a half" />-->
<!--        <label class="half" for="star8half" title="Pretty good - 8.5 stars"></label>-->
<!--        <input type="radio" id="star8" name="rating" value="8" />-->
<!--        <label class = "full" for="star8" title="Pretty good - 8 stars"></label>-->
<!--        <input type="radio" id="star7half" name="rating" value="7 and a half" />-->
<!--        <label class="half" for="star7half" title="Pretty good - 7.5 stars"></label>-->
<!--        <input type="radio" id="star7" name="rating" value="7" />-->
<!--        <label class = "full" for="star7" title="Pretty good - 7 stars"></label>-->
<!--        <input type="radio" id="star6half" name="rating" value="6 and a half" />-->
<!--        <label class="half" for="star6half" title="good - 6.5 stars"></label>-->
<!--        <input type="radio" id="star6" name="rating" value="6" />-->
<!--        <label class = "full" for="star6" title="good - 6 star"></label>-->
<!--        <input type="radio" id="star5half" name="rating" value="5 and a half" />-->
<!--        <label class="half" for="star5half" title="good - 5.5 stars">-->
<!--        </label> <input type="radio" id="star5" name="rating" value="5" />-->
<!--        <label class = "full" for="star5" title="good - 5 stars"></label>-->
<!--        <input type="radio" id="star4half" name="rating" value="4 and a half" />-->
<!--        <label class="half" for="star4half" title="Not bad-4.5 stars"></label>-->
<!--        <input type="radio" id="star4" name="rating" value="4" />-->
<!--        <label class = "full" for="star4" title="Not bad - 4 stars"></label>-->
<!--        <input type="radio" id="star3half" name="rating" value="3 and a half" />-->
<!--        <label class="half" for="star3half" title="Not bad - 3.5 stars"></label>-->
<!--        <input type="radio" id="star3" name="rating" value="3" />-->
<!--        <label class = "full" for="star3" title="Not bad - 3 stars"></label>-->
<!--        <input type="radio" id="star2half" name="rating" value="2 and a half" />-->
<!--        <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>-->
<!--        <input type="radio" id="star2" name="rating" value="2" />-->
<!--        <label class = "full" for="star2" title="Kinda bad - 2 stars"></label>-->
<!--        <input type="radio" id="star1half" name="rating" value="1 and a half" />-->
<!--        <label class="half" for="star1half" title="bad - 1.5 stars"></label>-->
<!--        <input type="radio" id="star1" name="rating" value="1" />-->
<!--        <label class = "full" for="star1" title="bad - 1 star"></label>-->
<!--        <input type="radio" id="starhalf" name="rating" value="half" />-->
<!--        <label class="half" for="starhalf" title="Very bad - 0.5 stars"></label>-->
<!--    </fieldset>-->
<!--    <br>-->
<!---->
<!--    <button style="float:right" class="btn btn-primary mb-3 " type="submit"  name="rev">Submit</button>-->

</form>

<br><br><br>

<footer style="background-color: midnightblue ; color: white; text-align: center ;padding: 20px">
    © 2021 Sony Interactive Entertainment LLC
</footer>

<script src="bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


</body>
</html>