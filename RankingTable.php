<!DOCTYPE html>
<head>
    <title>Ranking Table</title>
    <link rel="icon" href="Images/th.jpeg">
    <link rel="stylesheet" href="assets/animate.css">
    <link rel="stylesheet" href="assets/owl.carousel.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <style>

    </style>
</head>
<body style="opacity: 150% ; background-image: url(Images/motion_stripes-wide.jpg)">
<main>
    <!--start nav -->
    <?php
    include 'Nav.php';
    ?>
    <!--    end nav-->

    <br><br>

    <div class="container">
        <table class="table table-light">
            <thead>
            <tr>
                <th scope="col">Game</th>
                <th scope="col">Number of Players</th>
                <th scope="col">Category</th>
                <th scope="col">Rating</th>
                <
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Call of Duty : MW</th>
                <td>8 Million</td>
                <td>Action</td>
                <td>4.9 Stars</td>

            </tr>
            <tr>
                <th scope="row">Call of Duty: CW</th>
                <td>7.5 Million</td>
                <td>Action</td>
                <td>4.6 Stars</td>
            </tr>
            <tr>
                <th scope="row">FarCry 5</th>
                <td>6.8 Million</td>
                <td>Action & Adventure</td>
                <td>4.5 Stars</td>
            </tr>
            <tr>
                <th scope="row">FIFA 21</th>
                <td>6 Million</td>
                <td>Sports</td>
                <td>4.5 Stars</td>

            </tr>
            <tr>
                <th scope="row">Fortnite</th>
                <td>5.8 Million</td>
                <td>Action & Adventure</td>
                <td>4.4 Stars</td>

            </tr>
            <tr>
                <th scope="row">GTA V</th>
                <td>5.5 Million</td>
                <td>Action & Adventure</td>
                <td>4.3 Stars</td>

            </tr>
            <tr>
                <th scope="row">Rocket League</th>
                <td>5.2 Million</td>
                <td>Sports</td>
                <td>4.1 Stars</td>

            </tr>
            <tr>
                <th scope="row">rainbow Six Siege</th>
                <td>5.2 Million</td>
                <td>Action</td>
                <td>4.1 Stars</td>

            </tr>
            <tr>
                <th scope="row">OverWatch</th>
                <td>4.5 Million</td>
                <td>Adventure & Arcade</td>
                <td>3.9 Stars</td>

            </tr>
            <tr>
                <th scope="row">The Last of US</th>
                <td>3 Million</td>
                <td>Adventure</td>
                <td>3.5 Stars</td>

            </tr>
            </tbody>
        </table>
    </div>

    <br><br><br><br>

    <footer style="background-color: midnightblue ; color: white; text-align: center ;padding: 20px">
        © 2021 Sony Interactive Entertainment LLC
    </footer>
</main>


<script src="assets/owl.carousel.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</body>