 <?php

// Starting the session, to use and
// store data in session variable
session_start();

// If the session variable is empty, this
// means the user is yet to login
// User will be sent to 'login.php' page
// to allow the user to login
if (!isset($_SESSION['fname'])) {
    $_SESSION['msg'] = "You have to log in first";
    header('location: lognew.php');
}

// Logout button will destroy the session, and
// will unset the session variables
// User will be headed to 'login.php'
// after loggin out
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['fname']);
    header("location: lognew.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Homepage</title>
    <link rel="stylesheet" type="text/css"
          href="css.css">

  <?php include('server.php') ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>
            Login and Registration
            System - LAMP Stack
        </title>

        <link rel="stylesheet" type="text/css"
              href="CSS%20File.css">
    </head>
<body style="opacity: 150% ; background-image: url(Images/motion_stripes-wide.jpg)">
<div class="header">
    <h2>Login Here!</h2>
</div>

<form method="post" action="lognew.php">

    <?php include('error.php'); ?>

    <div class="input-group">
        <label>Enter Fname</label>
        <input type="text" name="fname"
               value="<?php echo $fname; ?>">
    </div>
    <div class="input-group">
        <label>Enter Password</label>
        <input type="password" name="password">
    </div>
    <div class="input-group">
        <button type="submit" class="btn"
                name="login_user">
            Login
        </button>
    </div>
    <p>
        New Here?
        <a href="regnew.php">
            Click here to regsiter!
        </a>
    </p>
</form>
</body>

</html>